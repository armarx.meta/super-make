#################################################################################
#scripts

set(list_armarx_dependencies "${CMAKE_SOURCE_DIR}/files/list-armarx-dependencies")
set(list_everything          "${CMAKE_SOURCE_DIR}/files/list-everything")
set(meta_make_generate       "${CMAKE_SOURCE_DIR}/files/meta-make-generate")

set(cfg_hosts               "${CMAKE_SOURCE_DIR}/config/host-core-ram-usecore")

file(GLOB_RECURSE fs "${CMAKE_SOURCE_DIR}/config/*" "${CMAKE_SOURCE_DIR}/files/*")

add_custom_target(files SOURCES ${fs})
#################################################################################
set(bd "${CMAKE_BINARY_DIR}")
set(sd "${CMAKE_SOURCE_DIR}")

set(dcc "${sd}/files/distcc")
set(icc "${sd}/files/icecc")

set(col "${sd}/files/collect")
set(gen "${sd}/files/generators")
set(tpl "${sd}/files/templates")
#################################################################################
#meta compilers
foreach(t setup start stop)
    add_custom_target(distcc.${t} WORKING_DIRECTORY "${bd}"  COMMAND "${dcc}/${t}")
    add_custom_target( icecc.${t} WORKING_DIRECTORY "${bd}"  COMMAND "${icc}/${t}")
endforeach()
#################################################################################
#create targets
function(make_meta targ)
    file(MAKE_DIRECTORY ${WRKD})
    configure_file("${tpl}/base.make.in" "${WRKD}/base.make" @ONLY)

   add_custom_target(${targ}
       COMMAND             $(MAKE) -f "${WRKD}/base.make" all
    )

#    set(part_run
#        COMMAND             LGREEN echo "run meta gen all"
#        COMMAND             $(MAKE) -f "${dir}/super-generate.make" all
#        COMMAND             LGREEN echo "run meta make $(TARG)"
#        COMMAND             $(MAKE) -f "${dir}/super-make.make" $(TARG)
#    )
#    set(part_run_distcc
#        COMMAND             "${distcc_start}"
#        ${part_run}.distcc
#        COMMAND             "${distcc_stop}"
#        DEPENDS             distcc_setup
#    )

#    add_custom_target(${targ}        ${part_gen_dir} ${part_argn} ${part_gen_meta} ${part_run}       )
#    add_custom_target(${targ}.distcc ${part_gen_dir} ${part_argn} ${part_gen_meta} ${part_run_distcc})
#    add_custom_target(${targ}.no_gen                                               ${part_run}       )
#    add_custom_target(${targ}.distcc.no_gen                                        ${part_run_distcc})
endfunction()

function(make_meta_ax pkg)
    set(SEARCH_CMD   "'${col}/armarx_dirs' '${pkg}'")
    set(GEN_LINK_CMD "${gen}/generate_makefile_run_link")
    set(WRKD         "${bd}/ax_${pkg}")
    set(GEND         "${gen}")

    make_meta(ax_${pkg})
endfunction()

function(make_meta_everything)
    set(SEARCH_CMD   "${col}/build_dirs")
    set(GEN_LINK_CMD "${gen}/generate_makefile_run_link")
    set(WRKD         "${bd}/everything")
    set(GEND         "${gen}")

    make_meta(everything)
endfunction()
#################################################################################
